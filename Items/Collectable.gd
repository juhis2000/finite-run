extends Area2D

var items = [
	"coin"
];
var item;

onready var controller = get_tree().get_root().get_node("World").get_node("Controller");

func _ready():
	item = items[0];

func _on_player_entered(body):
	if(item == "coin"):
		controller.coins += 1;
		queue_free();

func _on_VisibilityNotifier2D_screen_exited():
	queue_free();