extends KinematicBody2D

var state;
enum states {
	run, jump, double_jump, slide, die
	}
	
const UP = Vector2(0, -4);
const GRAVITY = 19; #20
const JUMP_HEIGHT = -300; #-400
const DOUBLE_JUMP_HEIGHT = -250; #-350
var motion = Vector2();
var jumps = 0;

func _ready():
	state = states.run;

func _process(delta):
	match state:
		states.run:
			_run(delta);
			_gravity(delta);
			
		states.jump:
			_jump(delta);
			_gravity(delta);
			
		states.double_jump:
			_double_jump(delta);
			_gravity(delta);
			
		states.slide:
			_slide(delta);
		states.die:
			_die();
			$Sprite.play("DIE");

func _gravity(delta):
	motion.y += GRAVITY;
	motion = move_and_slide(motion, UP);

func _run(delta):
	if(!is_on_floor()):
		if(jumps <= 1):
			if(motion.y < 0):
				$Sprite.play("JUMP");
			else:
				$Sprite.play("FALL");
		if(jumps > 1):
			$Sprite.play("DOUBLE_JUMP");
	else:
		$Sprite.play("RUN");
		if($Sprite.frame == 0 || $Sprite.frame == 2): $Sprite.offset.y -= 1;
		if($Sprite.frame == 0 || $Sprite.frame == 2): $Sprite.offset.y += 1;
	
	if(is_on_floor()):
		jumps = 0;
		if(Input.is_action_just_pressed("ui_jump")):
			jumps += 1;
			state = states.jump;
	elif(Input.is_action_just_pressed("ui_jump") && jumps < 2):
		jumps += 2;
		state = states.double_jump;
	
	if(position.x < -5):
		state = states.die;
	if(position.y > get_viewport().size.y/2 + 16):
		state = states.die;

func _jump(delta):
	motion.y = JUMP_HEIGHT;
	state = states.run;
	
func _double_jump(delta):
	motion.y = DOUBLE_JUMP_HEIGHT;
	state = states.run;

func _die():
	get_tree().paused = true;
	if(Input.is_action_pressed("ui_jump")):
		get_tree().paused = false;
		get_tree().change_scene("res://Main/World.tscn");
		
	if(Input.is_action_just_pressed("ui_quit")):
		get_tree().quit()
		
	if(position.y < 300):
		$PlatformCollider.disabled = true;
		position.y += 3;