extends Camera2D

var platforms_list = [];
var target;
var previous_pos = 0;

func _process(delta):	
	var platforms = get_tree().get_nodes_in_group("PLATFORM");
	for platform in platforms:
		if(!platforms_list.find(platform)):
			platforms_list.append(platform);
		if(platform.position.x <= 0):
			platforms_list.erase(platform);
		else:
			target = platform;
			if(target != null && platforms.find(target)):
				if(target.position.y < offset.y):
					offset.y -= 1;
				if(target.position.y > offset.y + 20):
					offset.y += 1;