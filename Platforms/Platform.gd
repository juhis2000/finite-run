extends StaticBody2D

var speed = 100;
var color = 1;
var sprites = [];

func _ready():
	for node in get_children():
		if node is Sprite:
			sprites.append(node);

func _process(delta):
	position.x -= speed * delta;

	if(position.x + $Sprite.texture.get_size().x/2 + 32 < 0):
		if(!$Collider.disabled): $Collider.disabled = true;
		position.y += speed * delta;
		if(color > 0.2):
			color -= 0.02;
			for sprite in sprites:
				sprite.set_modulate(Color(color, color, color));

func _on_VisibilityNotifier2D_screen_exited():
	queue_free();