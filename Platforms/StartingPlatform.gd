extends StaticBody2D

var speed = 100;

func _process(delta):
	position.x -= speed * delta;
	
	if(position.x + 300 < -get_viewport().size.x/2 + $Sprite.texture.get_size().x):
		queue_free();