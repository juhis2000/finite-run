extends Node

onready var viewport = get_viewport();

var platforms = [
		load("res://Platforms/BasicPlatform.tscn")
		];
var platform_y_position;

var collectables = load("res://Items/Collectable.tscn")

var time = 0;

func _ready():
	randomize();
	_spawn_platforms();

func _process(delta):
	if(time <= 0.7): time += delta;
	else:
		time = 0;
		_spawn_platforms();
		#_spawn_collectables();

func irand_range(x, y):
	return (randi() % (y - x + 1) + x);
	
func _spawn_platforms():
	var platform = platforms[irand_range(0, platforms.size()-1)].instance();
	add_child(platform);
	platform.position = Vector2(viewport.size.x/2 + platform.get_node("Sprite").texture.get_size().x, irand_range(-50, 50));
	platform_y_position = platform.position.y - platform.get_node("Sprite").texture.get_size().y;
	
func _spawn_collectables():
	if(irand_range(0, 1) == 0): return;
	var collectable = collectables.instance();
	add_child(collectable);
	collectable.item = collectable.items[irand_range(0, collectable.items.size()-1)];
	collectable.position = Vector2(viewport.size.x/2 + 32, platform_y_position);