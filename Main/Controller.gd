extends Node

var coins = 0;

var state;
enum states {
	idle, pause
	}

func _ready():
	state = states.idle;

func _process(delta):
	match state:
		states.idle:
			_idle();
		states.pause:
			_pause();
			
func _idle():
	if(Input.is_action_just_pressed("ui_pause")):
		state = states.pause;
		
func _pause():
	get_tree().paused = !get_tree().paused;
	state = states.idle;