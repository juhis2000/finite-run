extends StaticBody2D

var speed = 1;

func _ready():
	pass

func _process(delta):
	position.x -= speed;

func _on_VisibilityNotifier2D_screen_exited():
	queue_free();