extends Node

var bg_nodes = [preload("res://Background/Levels/Background.tscn"),
preload("res://Background/Levels/Background2.tscn"),
preload("res://Background/Levels/Background3.tscn")];
var nodes = [];

func _ready():
	randomize();
	var bg = bg_nodes[0].instance();
	add_child(bg);
	bg.position.x = 150;
	nodes.append(bg);
	
	var bg2 = bg_nodes[0].instance();
	add_child(bg2);
	bg2.position.x = 10;

func _process(delta):
	if(nodes.size() > 0):
		if(nodes[nodes.size()-1].position.x + nodes[nodes.size()-1].get_node("Sprite").texture.get_size().x/2 <= get_viewport().size.x):
			_spawn();

func irand_range(x, y):
	return (randi() % (y - x + 1) + x);

func _spawn():
	var num = irand_range(0, bg_nodes.size()-1);
	var bg = bg_nodes[num].instance();
	add_child(bg);
	bg.position.x = get_viewport().size.x + bg.get_node("Sprite").texture.get_size().x/2 - 1;
	nodes.append(bg);