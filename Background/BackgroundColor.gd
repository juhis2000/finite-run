extends Sprite

var a = 0.3;
var dir = 1;

func _ready():
	pass

func _process(delta):
	if(dir == 1):
		if(a < 0.6): a += 0.01;
		else: dir = -1;
	else:
		if(a >= 0.2): a -= 0.01;
		else: dir = 1;
	
	position = get_parent().get_parent().get_node("Camera2D").offset;
	get_material().set_shader_param("modulate", Color(1, 0.28, 0.32, a));