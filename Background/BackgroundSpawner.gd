extends Node

var bg_node = preload("res://Background/Background.tscn");
var nodes = [];

var i = 0;
var pos = [];

func _ready():
	var bg = bg_node.instance();
	add_child(bg);
	bg.position = Vector2();
	i += 1;
	nodes.append(bg);

func _process(delta):
	if(nodes.size() > 0):
		if(nodes[nodes.size()-1].position.x + nodes[nodes.size()-1].get_node("Sprite").texture.get_size().x):
			_spawn();

func _spawn():
	var bg = bg_node.instance();
	add_child(bg);
	bg.position.x = get_viewport().size.x;# + bg.get_node("Sprite").texture.get_size().x;
	nodes.append(bg);

#extends Node
#
#export (Color)var color;
#export (float)var speed;
#export (int)var min_w;
#export (int)var max_w;
#export (int)var min_h;
#export (int)var max_h;
#export (int)var gap_fill;
#export (int)var start_rectangles;
#
#var w = 0;
#var h = 0;
#var tex = preload("res://Background/rectangle.png");
#var nodes = [];
#var i;
#
#func _ready():
#	i = -get_viewport().size.x;
#	randomize();
#	for a in start_rectangles:
#		i += min_w;
#		create_rect(Vector2(i, get_viewport().size.y), color);
#
#func irand_range(x, y):
#	return (randi() % (y - x + 1) + x);
#
#func create_rect(pos, clr):
#	var node = Sprite.new();
#	add_child(node);
#	nodes.append(node);
#
#	w = irand_range(min_w, max_w);
#	h = irand_range(min_h, max_h);
#
#	node.set_material(load("res://Background/Glow.material"))
#
#	node.z_index = -1;
#	node.centered = false
#	node.offset = Vector2(0,0)
#	node.texture = tex;
#	node.set_modulate(clr);
#	node.position = Vector2(pos.x, pos.y - h);
#	node.region_enabled = true;
#	node.set_region_rect(Rect2(0, 0, w, h));
#
#func _process(delta):
#	if(nodes.size() > 0):
#		for node in nodes:
#			if(node.position.x + node.get_region_rect().size.x < -get_viewport().size.x/2):
#				nodes.erase(node);
#				node.queue_free();
#			else:
#				node.position.x -= speed;
#		if(nodes[nodes.size()-1].position.x + nodes[nodes.size()-1].get_region_rect().size.x - gap_fill <= get_viewport().size.x):
#			create_rect(get_viewport().size, color);