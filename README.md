I created this project to practice infinite runner mechanics such as looping
background and spawning random platforms. Created in Godot 3 and GDScript. All
the code and sprites are made by me. This project is one of my first projects
where I use finite state machine.  
Gameplay  
The player's goal is to run as far as possible without falling in a lava pit
while collecting coins.   
Video of the gameplay  
![](video.mp4)

See also https://godotengine.org/license