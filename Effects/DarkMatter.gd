extends Particles2D

var start_pos;

func _ready():
	start_pos = position.x;
	position.x -= 100;

func _process(delta):
	if(position.x <= start_pos+120): position.x += 0.5;